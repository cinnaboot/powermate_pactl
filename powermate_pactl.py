#!/usr/bin/python


import glob
import time

from lib.powermate import powermate
from lib.python_pulse_control.pulsectl import pulsectl


RETRY_ATTEMPTS = 10
INCREASE_AMOUNT = 0.01


class PowerMate(powermate.PowerMateBase):

    def __init__(self, path):
        self.pulse = pulsectl.Pulse()
        super().__init__(path)

    def rotate(self, rotation):
        if (rotation > 0):
            self.change_vol(INCREASE_AMOUNT)
        else:
            self.change_vol(-INCREASE_AMOUNT)

    def change_vol(self, vol):
        for sink in self.pulse.sink_list():
            self.pulse.volume_change_all_chans(sink, vol)

def start():
    for i in range(1, RETRY_ATTEMPTS + 1):
        path = ''
        powermates = glob.glob('/dev/input/by-id/*PowerMate*')

        if len(powermates) > 0 :
            path = powermates[0]
            print(f'found powermate at {path}')

        if path:
            pm = PowerMate(path)

            try:
                pm.run()
            except (KeyboardInterrupt, SystemExit):
                return
            except Exception as e:
                print(f'caught exception from AsyncFileEventDispatcher.run(): {e}')
                print('attempting restart')
                time.sleep(5) # NOTE: give time to enumerate USB after suspend
                start()
                return
        elif i == 1:
            print(f'waiting up to {RETRY_ATTEMPTS}s for powermate')
        elif i == RETRY_ATTEMPTS:
            print(f'giving up looking for powermate after {RETRY_ATTEMPTS}s')

        time.sleep(1)

if __name__ == '__main__':
    start()
